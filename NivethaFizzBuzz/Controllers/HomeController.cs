﻿using NivethaFizzBuzz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace NivethaFizzBuzz.Controllers
{
    public class HomeController : Controller
    {
       
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzViewModel());
        }


        [HttpPost]
        public ActionResult Index(FizzBuzzViewModel model, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
         

            if (ModelState.IsValid)
            {
                           
                return RedirectToAction("FizzBuzz", new { value = model.UserValue, page = 1 });
            }
            else
                return View(model);
        }

        [HttpGet]
        public ActionResult FizzBuzz(int value, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);

            var model = new FizzBuzzViewModel();
            model.UserValue = value;
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            model.FizzBuzzPagination = fizzBuzz.GetFizzBuzz(model.UserValue).ToPagedList(pageNumber, pageSize);

            return View(model);


        }

        [HttpGet]
        public ActionResult UserInput( int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            FizzBuzzRequestData.InputPagination = FizzBuzzRequestData.UserInputs.ToPagedList(pageNumber, pageSize);

            return View(FizzBuzzRequestData.UserInputs);


        }
    }
}