﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NivethaFizzBuzz.Startup))]
namespace NivethaFizzBuzz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
