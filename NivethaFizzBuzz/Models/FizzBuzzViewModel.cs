﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NivethaFizzBuzz.Models
{
    //step 1
    public class FizzBuzzViewModel
    {
        [Range(1, 1000)]
        [Required]
        [DisplayName("Value")]
        public int UserValue { get; set; }
        public IPagedList<String> FizzBuzzPagination { get; set; }
    }
     public static class FizzBuzzRequestData
     {
         public static List<string> UserInputs {get;set;}
         public static IPagedList<String> InputPagination { get; set; }
     }
}