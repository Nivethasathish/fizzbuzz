﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NivethaFizzBuzz.Models
{
    //Step 2
    public class FizzBuzz
    {
        public static List<string> UserRequest =new List<string>();
        //Dependency injection by DateTime.Now as a func instead of complicating with a time class
        private readonly Func<DateTime> today;

        public FizzBuzz(Func<DateTime> now)
        {
            today = now;
        }

        public List<string> GetFizzBuzz(  int value)
        {
             if (value < 1 || value > 1000)
                throw new ArgumentException("Value should be greater than 0 and less than or equal to 1000");

            UserRequest.Add (value.ToString ());
            var numbers = new List<string>();

            for (var i = 1; i <= value; i++)
            {
                numbers.Add(GetFizzBuzzValue(i));
            }
            FizzBuzzRequestData.UserInputs = UserRequest;
            return numbers;

         }
        public string GetFizzBuzzValue(int i)
        {
            if (i < 1 || i > 1000)
                 return "Value should be greater than 0 and less than or equal to 1000";
            if (i % 3 == 0 && i % 5 == 0)
            {
                if (today().DayOfWeek == DayOfWeek.Thursday)
                    return "WizzWuzz";
                else
                    return "FizzBuzz";
            }
            else if (i % 3 == 0)
            {
                if (today().DayOfWeek == DayOfWeek.Thursday)
                    return "Wizz";
                else
                    return "Fizz";
            }
            else if (i % 5 == 0)
            {
                if (today().DayOfWeek == DayOfWeek.Thursday)
                    return "Wuzz";
                else
                    return "Buzz";
            }
            else
            {
                return i.ToString();
            }
        }
        //private string GetFizzBuzzValue(int i)
        //{
        //    string returnValue;
        //    if (i % 3 == 0 && i % 5 == 0)
        //    {
        //        switch (today().DayOfWeek.ToString().ToUpper())
        //        {
        //            case "MONDAY":
        //                returnValue = "MizzMuzz";
        //                break;
        //            case "TUESDAY":
        //                returnValue = "TizzTuzz";
        //                break;
        //            case "WEDNESDAY":
        //                returnValue = "WizzWuzz";
        //                break;
        //            case "THURSDAY":
        //                returnValue = "ThizzThuzz";
        //                break;
        //            case "FRIDAY":
        //                returnValue = "FizzFuzz";
        //                break;
        //            default :
        //                returnValue = "FizzBuzz";
        //                break;
        //        }
               
        //    }
        //    else if (i % 3 == 0)
        //    {
        //        if (today().DayOfWeek == DayOfWeek.Wednesday)
        //            returnValue = "Wizz";
        //        else
        //            returnValue = "Fizz";
        //    }
        //    else if (i % 5 == 0)
        //    {
        //        if (today().DayOfWeek == DayOfWeek.Wednesday)
        //            returnValue = "Wuzz";
        //        else
        //            returnValue = "Buzz";
        //    }
        //    else
        //    {
        //        returnValue = i.ToString();
        //    }
        //    return returnValue;
        //}
    }
}