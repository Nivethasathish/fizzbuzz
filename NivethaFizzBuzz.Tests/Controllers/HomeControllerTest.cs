﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using NivethaFizzBuzz;
using NivethaFizzBuzz.Controllers;
using NUnit.Framework;
using NivethaFizzBuzz.Models;

namespace NivethaFizzBuzz.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        
        [Test]
        public void CheckReturnsTheNumberForNonMulipleOf3Or5()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("7", fizzBuzz.GetFizzBuzzValue(7));
        }

        [Test]
        public void CheckReturnsFizzForMultipleOf3()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Fizz", fizzBuzz.GetFizzBuzzValue(6));
        }

        [Test]
        public void CheckReturnsBuzzForMultipleOf5()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Buzz", fizzBuzz.GetFizzBuzzValue(10));
        }

        [Test]
        public void CheckReturnsFizzBuzzForMultipleOf3And5()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("FizzBuzz", fizzBuzz.GetFizzBuzzValue(30));
        }

        [Test]
        public void CheckReturns1For1()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("1", fizzBuzz.GetFizzBuzzValue(1));
        }

        [Test]
        public void CheckReturns2For2()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("2", fizzBuzz.GetFizzBuzzValue(2));
        }

        [Test]
        public void CheckReturnsFizzFor3()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Fizz", fizzBuzz.GetFizzBuzzValue(3));
        }

        [Test]
        public void CheckReturnsBuzzFor5()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Buzz", fizzBuzz.GetFizzBuzzValue(5));
        }

        [Test]
        public void CheckThrowsArgumentOfRangeExceptionFor0()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Value should be greater than 0 and less than or equal to 1000", fizzBuzz.GetFizzBuzzValue(0));
            
        }

        [Test]
        public void CheckThrowsArgumentOfRangeExceptionForValueLessThan0()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Value should be greater than 0 and less than or equal to 1000", fizzBuzz.GetFizzBuzzValue(-1));
           
        }

        [Test]
        public void CheckThrowsArgumentOfRangeExceptionForValueGreaterThan1000()
        {
            var fizzBuzz = new FizzBuzz(() => DateTime.UtcNow);
            Assert.AreEqual("Value should be greater than 0 and less than or equal to 1000", fizzBuzz.GetFizzBuzzValue(1001));
           
        }

    }
}
